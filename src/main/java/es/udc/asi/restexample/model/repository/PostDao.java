package es.udc.asi.restexample.model.repository;

import java.util.List;

import es.udc.asi.restexample.model.domain.Post;
import es.udc.asi.restexample.model.service.dto.PostSortType;

public interface PostDao {
  List<Post> findAll(String filter, PostSortType sort);

  Post findById(Long id);

  void create(Post post);

  void update(Post post);

  void deleteById(Long id);

  List<Post> findAllByTag(Long tagId);
}
