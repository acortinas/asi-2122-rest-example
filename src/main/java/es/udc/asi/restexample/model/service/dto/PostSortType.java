package es.udc.asi.restexample.model.service.dto;

public enum PostSortType {
  MOST_RECENT, LESS_RECENT, AUTHOR_NAME
}
