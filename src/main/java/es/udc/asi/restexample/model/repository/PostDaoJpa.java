package es.udc.asi.restexample.model.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import es.udc.asi.restexample.model.domain.Post;
import es.udc.asi.restexample.model.repository.util.GenericDaoJpa;
import es.udc.asi.restexample.model.service.dto.PostSortType;

import javax.persistence.TypedQuery;

@Repository
public class PostDaoJpa extends GenericDaoJpa implements PostDao {

  @Override
  public List<Post> findAll(String filter, PostSortType sort) {
    String queryStr = "select p from Post p";
    if (filter != null) {
      queryStr += " where p.title like :filter or p.body like :filter or p.author.login like :filter";
    }

    String sortStr = "p.id asc";
    if (sort != null) {
      switch (sort) {
        case AUTHOR_NAME:
          sortStr = "p.author.login asc";
          break;
        case LESS_RECENT:
          sortStr = "p.timestamp asc";
          break;
        case MOST_RECENT:
          sortStr = "p.timestamp desc";
          break;
      }
    }
    queryStr += " order by " + sortStr;

    TypedQuery<Post> query = entityManager.createQuery(queryStr, Post.class);
    if (filter != null) {
      query.setParameter("filter", "%" + filter + "%");
    }

    return query.getResultList();
  }

  @Override
  public Post findById(Long id) {
    return entityManager.find(Post.class, id);
  }

  @Override
  public void create(Post post) {
    entityManager.persist(post);
  }

  @Override
  public void update(Post post) {
    entityManager.merge(post);
  }

  @Override
  public void deleteById(Long id) {
    Post post = findById(id);
    delete(post);
  }

  @Override
  public List<Post> findAllByTag(Long tagId) {
    return entityManager.createQuery("select p from Post p join p.tags pt where pt.id = :tagId", Post.class)
        .setParameter("tagId", tagId).getResultList();
  }

  private void delete(Post post) {
    entityManager.remove(post);
  }
}
